package project_euler;

import io.vavr.collection.List;
import lombok.extern.java.Log;
import lombok.val;
import maths.integers.IntegerFunction;

import java.math.BigInteger;

@Log
public class P3 {

    public static void main(String[] args) {
        val testNumber = BigInteger.valueOf(600851475143L);
        List<BigInteger> primefactors = IntegerFunction.primeFactors(testNumber);

        log.info("Result=" + primefactors.last());
    }
}

package project_euler;

import lombok.extern.java.Log;
import lombok.val;
import maths.integers.IntegerFunction;

import java.math.BigInteger;
import java.util.function.Predicate;

import static maths.integers.IntegerFunction.ZERO;

@Log
public class P1 {

    public static void main(String[] args) {
        val three = BigInteger.valueOf(3L);
        val five = BigInteger.valueOf(5L);

        Predicate<BigInteger> filter = a -> a.mod(three).equals(ZERO) || a.mod(five).equals(ZERO);

        BigInteger result = IntegerFunction
                .filterAndSumUoTo(BigInteger.valueOf(1000L), filter);

        log.info("Result is:" + result);
    }
}

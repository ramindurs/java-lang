package project_euler;

import io.vavr.collection.List;
import lombok.extern.java.Log;
import lombok.val;
import maths.integers.IntegerFunction;

import java.math.BigInteger;


import static maths.integers.IntegerFunction.EVENFILTER;
import static maths.integers.IntegerFunction.ZERO;

@Log
public class P2 {

    private static final BigInteger MAX = BigInteger.valueOf(4000000L);

    public static void main(String[] args) {
        List<BigInteger> fibonacci = IntegerFunction.fibonacciBelow(MAX);

        val result = fibonacci.filter(EVENFILTER).fold(ZERO, BigInteger::add);

        log.info("Result is: " + result);
    }
}

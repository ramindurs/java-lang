package maths.sets;

import java.math.BigInteger;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * A class with static methods that results in sets of integers representing Z.
 */
public class IntegerSet {

    private static IntegerSet instance = new IntegerSet();

    public static IntegerSet getInstance() {
        return instance;
    }

    private IntegerSet() {
    }

    /**
     * Gets a Set of 1 to 100 numbers.
     *
     * @return a set of Z100
     */
    public static Set<Integer> getZ100() {
        return getSetInRange(1, 100);
    }

    /**
     * Gets a set of integers in range.
     *
     * @param startInclusive inclusive start range
     * @param endInclusive   inclusive end range
     * @return the set
     */
    public static Set<Integer> getSetInRange(Integer startInclusive, Integer endInclusive) {
        return IntStream.rangeClosed(startInclusive, endInclusive).boxed().collect(Collectors.toSet());
    }

    /**
     * Gets a set of BigInts in range.
     *
     * @param startInclusive inclusive start range
     * @param endInclusive   inclusive end range
     * @return the set
     */
    public static Set<BigInteger> getSetInRange(BigInteger startInclusive, BigInteger endInclusive) {
        Stream<BigInteger> infiniteStream = Stream.iterate(startInclusive, i -> i.add(BigInteger.ONE));
        return infiniteStream.limit(endInclusive.subtract(startInclusive).longValueExact()).collect(Collectors.toSet());
    }
}

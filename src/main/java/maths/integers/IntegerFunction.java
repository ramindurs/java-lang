package maths.integers;

import fp.recursion.Trampoline;
import io.vavr.collection.List;
import lombok.val;
import maths.sets.IntegerSet;

import java.math.BigInteger;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static fp.recursion.TrampolineCaller.call;
import static fp.recursion.TrampolineCaller.done;

public class IntegerFunction {

    private static IntegerFunction instance = new IntegerFunction();

    public static IntegerFunction getInstance() {
        return instance;
    }

    private IntegerFunction() {
    }

    public static final BigInteger ZERO = BigInteger.ZERO;
    public static final BigInteger ONE = BigInteger.ONE;
    public static final BigInteger TWO = BigInteger.TWO;

    public static final Predicate<BigInteger> EVENFILTER = a -> a.mod(TWO).equals(ZERO);


    public static Set<BigInteger> marsennePrime(final Set<Integer> primes) {
        return marsenneNumber(primes).stream().filter(IntegerFunction::isPrime).collect(Collectors.toSet());

    }

    public static Set<BigInteger> marsenneNumber(final Set<Integer> primes) {
        return primes.stream().map(IntegerFunction::calculateMarsenneNumber).collect(Collectors.toSet());
    }

    private static BigInteger calculateMarsenneNumber(final Integer prime) {
        return BigInteger.valueOf(2).pow(prime).subtract(BigInteger.ONE);
    }

    public static Set<Integer> primeFilter(final Set<Integer> numbers) {
        return numbers.stream().filter(IntegerFunction::isPrime).collect(Collectors.toSet());
    }

    private static boolean isPrime(final BigInteger bigInt) {
        if (bigInt.equals(BigInteger.ONE) || bigInt.equals(BigInteger.TWO))
            return true;
        else {
            Set<BigInteger> divisors = IntegerSet.getSetInRange(BigInteger.TWO, bigInt.sqrt());
            return !doesDivide(divisors, bigInt);
        }
    }

    private static boolean isPrime(final Integer n) {
        if (n == 1 || n == 2)
            return true;
        else {
            Set<Integer> divisors = IntegerSet.getSetInRange(2, (int) Math.round(Math.sqrt(n)));
            return !doesDivide(divisors, n);
        }
    }

    /**
     * This is an early version of recursive function. This will cause stackoverflow when dealing with large numbers.
     *
     * @param divisors
     * @param n
     * @return
     */
    private static boolean doesDivide(final Set<BigInteger> divisors, final BigInteger n) {
        if (divisors.isEmpty())
            return false;
        else {
            BigInteger head = divisors.stream().findFirst().get();
            if (n.mod(head).equals(BigInteger.ZERO)) {
                return true;
            } else {
                Set<BigInteger> reducedSet = divisors.stream().filter(a -> a.mod(head).equals(BigInteger.ZERO))
                        .collect(Collectors.toSet());
                return doesDivide(reducedSet, n);
            }
        }
    }

    private static boolean doesDivide(final Set<Integer> divisors, final Integer n) {
        if (divisors.isEmpty())
            return false;
        else {
            Integer head = divisors.stream().findFirst().get();
            if (n % head == 0)
                return true;
            else {
                Set<Integer> reducedSet = divisors.stream().filter(a -> a % head != 0).collect(Collectors.toSet());
                return doesDivide(reducedSet, n);
            }
        }
    }

    public static BigInteger factorial(BigInteger n) {
        Trampoline<BigInteger, BigInteger> factorialTC = trampolineFactorial(ONE, n);
        return factorialTC.invoke();
    }

    private static Trampoline<BigInteger, BigInteger> trampolineFactorial(BigInteger f, BigInteger n) {
        return n.equals(ONE) ? done(f) : call(() -> trampolineFactorial(f.multiply(n), n.subtract(ONE)));
    }

    /**
     * Filters out numbers and then sums all remaining numbers from 1 to a given number
     *
     * @param upto   the number upto
     * @param filter the filter
     * @return the sum of remaining numbers
     */
    public static BigInteger filterAndSumUoTo(BigInteger upto, Predicate<BigInteger> filter) {
        val numbers = IntegerSet.getSetInRange(BigInteger.ONE, upto);
        return numbers.stream().filter(filter).reduce(ZERO, BigInteger::add);
    }

    /**
     * Generates a list of Fibonacci numbers below the given maximum
     *
     * @param max the maximum
     * @return list of Fibonacci
     */
    public static List<BigInteger> fibonacciBelow(BigInteger max) {
        if (max.equals(ONE)) {
            return List.of(ONE);
        }
        if (max.equals(TWO)) {
            return List.of(ONE, ONE);
        }
        Trampoline<BigInteger, List<BigInteger>> fib = trampolineFibonacci(BigInteger.ONE, BigInteger.ONE, max, List.of(ONE, ONE));
        return fib.invoke();
    }

    private static Trampoline<BigInteger, List<BigInteger>> trampolineFibonacci(BigInteger n1, BigInteger n2, BigInteger max, List<BigInteger> accum) {
        return max.compareTo(n2) <= 0 ? done(accum) : call(() -> trampolineFibonacci(n2, n1.add(n2), max.subtract(ONE), accum.append(n1.add(n2))));
    }

    public static List<BigInteger> primeFactors(BigInteger n) {
        if (n.equals(ONE) || n.equals(TWO))
            return List.of(n);
        Set<BigInteger> set = IntegerSet.getSetInRange(TWO, n.sqrt());
        List<BigInteger> list = List.ofAll(set.stream());
        Trampoline<List<BigInteger>, List<BigInteger>> t = trampolinePrimeFactors(list, List.empty(), n);
        return t.invoke();
    }

    private static Trampoline<List<BigInteger>, List<BigInteger>> trampolinePrimeFactors(List<BigInteger> remainingNumbers, List<BigInteger> primeFactors, BigInteger n) {
        return remainingNumbers.isEmpty() ?
                primeFactors.isEmpty() ? done(List.of(n)) : done(primeFactors)
                : call(() -> n.mod(remainingNumbers.head()).equals(BigInteger.ZERO) ?
                trampolinePrimeFactors(remainingNumbers.filter(a -> !a.mod(remainingNumbers.head()).equals(BigInteger.ZERO)), primeFactors.append(remainingNumbers.head()), n)
                : trampolinePrimeFactors(remainingNumbers.filter(a -> !a.mod(remainingNumbers.head()).equals(ZERO)), primeFactors, n));
    }
}

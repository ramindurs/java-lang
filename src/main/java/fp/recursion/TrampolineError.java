package fp.recursion;

/**
 * A general exception class for trampoline exceptions. In general,
 * thare are only two types of exception:
 * 1. When there is no further actions to carry out
 * 2. When there is no results
 */
public class TrampolineError extends Error {
    public TrampolineError(String msg) {
        super(msg);
    }
}

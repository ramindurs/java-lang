package fp.recursion;

import java.util.function.Function;
import java.util.function.Predicate;

public class TrampolineCaller {

    private TrampolineCaller() {
    }

    public static <A, B> Trampoline<A, B> call(Trampoline<A, B> nextCall) {
        return nextCall;
    }

    public static <A, B> Trampoline<A, B> done(B value) {
        return new Trampoline<A, B>() {

            @Override
            public boolean isComplete() {
                return true;
            }

            @Override
            public B result() {
                return value;
            }

            @Override
            public Trampoline<A, B> apply() {
                throw new TrampolineError("No further action");
            }
        };
    }
}

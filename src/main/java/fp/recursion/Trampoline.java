package fp.recursion;

import java.util.Optional;
import java.util.stream.Stream;

@FunctionalInterface
public interface Trampoline<A, B> {

    Trampoline<A, B> apply();

    default boolean isComplete() {
        return false;
    }

    default B result() {
        throw new TrampolineError("Result is not available");
    }

    default B invoke() {
        return Stream.iterate(this, Trampoline::apply)
                .filter(Trampoline::isComplete)
                .findFirst()
                .map(Trampoline::result)
                .get();
    }
}

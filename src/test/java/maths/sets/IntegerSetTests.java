package maths.sets;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static maths.sets.IntegerSet.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class IntegerSetTests {


    @Test
    @DisplayName("It should give a set of Z100")
    public void testZ100(){
        Set<Integer> z100 = getZ100();

        assertNotNull(z100);
        assertEquals(100, z100.size());
    }
}

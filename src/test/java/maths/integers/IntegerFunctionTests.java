package maths.integers;

import io.vavr.collection.List;
import maths.sets.IntegerSet;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.Set;

import static maths.integers.IntegerFunction.*;
import static org.junit.jupiter.api.Assertions.*;

public class IntegerFunctionTests {

    private final Set<Integer> numbers = IntegerSet.getZ100();

    @Test
    @DisplayName("It should filter out all non-prime numbers")
    public void testPrimeFilter() {
        Set<Integer> primes = primeFilter(numbers);

        assertEquals(26, primes.size());
    }

    @Test
    @DisplayName("It should give a set of Marsenne numbers")
    public void testMarsenneNumber() {
        Set<Integer> primes = primeFilter(numbers);

        Set<BigInteger> marsenneNumbers = marsenneNumber(primes);

        assertEquals(26, marsenneNumbers.size());
        assertTrue(marsenneNumbers.contains(BigInteger.valueOf(2147483647l)));
    }

    /**
     * We expect this test to fail with a standard recursion.
     */
    @Test
    @DisplayName("It should throw SOE for recursive calls")
    public void testThatItThrowsSOEForRecursiveCalls() {
        assertThrows(StackOverflowError.class, () -> {
            Set<Integer> primes = primeFilter(numbers);

            marsennePrime(primes);
        });
    }

    @Test
    @DisplayName("It should return factorial of very large integers without throwing SOE")
    public void testFactorial() {
        BigInteger actual = factorial(BigInteger.valueOf(20000L));

        assertTrue(actual.compareTo(BigInteger.ONE) > 0);
    }

    @Test
    @DisplayName("Fibonacci MAX should return 1 when max is one")
    public void testFibonacciToMaximumOfOne() {
        List<BigInteger> fib = fibonacciBelow(ONE);

        asertListSizeAndLastElementSize(fib, 1, ONE);
    }

    @Test
    @DisplayName("Fibonacci MAX should return [1, 1] when max is two")
    public void testFibonacciToMaximumOfTwo() {
        List<BigInteger> fib = fibonacciBelow(TWO);

        asertListSizeAndLastElementSize(fib, 2, ONE);
    }

    @Test
    @DisplayName("Fibonacci MAX should return [1,1,2] when max is three")
    public void testFibonacciToMaxThree() {
        List<BigInteger> fib = fibonacciBelow(BigInteger.valueOf(3));

        asertListSizeAndLastElementSize(fib, 3, TWO);
    }

    @Test
    @DisplayName("Fibonacci MAX should return [1,1,2,3,5] when max is 7")
    public void testFibonacciToMaxSeven() {
        List<BigInteger> fib = fibonacciBelow(BigInteger.valueOf(7));

        asertListSizeAndLastElementSize(fib, 5, BigInteger.valueOf(5));
    }

    @Test
    @DisplayName("PrimeFactor gets a list of prime factors when n is 1")
    public void testPrimeFactorListWhenOne() {
        List<BigInteger> pfs = IntegerFunction.primeFactors(ONE);

        asertListSizeAndLastElementSize(pfs, 1, ONE);
    }

    @Test
    @DisplayName("PrimeFactor gets a list of prime factors when n is 2")
    public void testPrimeFactorListWhenTwo() {
        List<BigInteger> pfs = IntegerFunction.primeFactors(TWO);

        asertListSizeAndLastElementSize(pfs, 1, TWO);
    }

    @Test
    @DisplayName("PrimeFactor gets a list of prime factors when n is 98")
    public void testPrimeFactorListWhenNIsProductOfPrimes() {
        List<BigInteger> pfs = IntegerFunction.primeFactors(BigInteger.valueOf(98));

        asertListSizeAndLastElementSize(pfs, 2, BigInteger.valueOf(7));
    }

    private void asertListSizeAndLastElementSize(List list, int listSize, BigInteger lastElement) {
        assertEquals(listSize, list.size());
        assertEquals(lastElement, list.last());
    }

}
